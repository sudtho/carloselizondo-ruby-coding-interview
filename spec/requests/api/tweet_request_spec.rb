require 'rails_helper'

RSpec.describe "API Tweets", type: :request do
  let!(:user) { create(:user) }
  let!(:valid_tweet) { {body: Faker::Lorem.characters(number: 10), user_id: user.id} }
  let!(:invalid_tweet) { {body: Faker::Lorem.characters(number: 181), user_id: user.id} }

  describe "#create" do
    let(:result) { JSON.parse(response.body) }

    context 'when creating a tweet' do
      it 'validates tweets length with valid character length' do
        post api_tweets_path(valid_tweet)
        
        expect(response.code).to eq("200")
      end

      it 'validates tweets length with invalid character length' do
        post api_tweets_path(invalid_tweet)
        
        expect(response.code).to eq("400")
      end
    end
  end
end
