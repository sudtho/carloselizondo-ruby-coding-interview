class Tweet < ApplicationRecord
  belongs_to :user

  validates :body, length: { maximum: 180 }
end
